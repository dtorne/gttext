;NSIS Modern User Interface
;Start Menu Folder Selection Example Script
;Written by Joost Verburg

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;Includes
!include "nsProcess.nsh"
!include "FileFunc.nsh"



# MultiUser Symbol Definitions
;multiuser 
!define MULTIUSER_EXECUTIONLEVEL Admin
!define MULTIUSER_MUI
!define MULTIUSER_INSTALLMODE_COMMANDLINE
!define MULTIUSER_INSTALLMODE_DEFAULT_REGISTRY_KEY "Software\Softocr\InstMode"
!define MULTIUSER_INSTALLMODE_DEFAULT_REGISTRY_VALUENAME MultiUserInstallMode
!define MULTIUSER_INSTALLMODE_INSTDIR "Softocr"
!define MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_KEY "Software\Softocr\InstDir"
!define MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_VALUE "Path"



	

!include MultiUser.nsh


;General

  !define MUI_ARQUITECTURE "32"
 ; !define MUI_PROGRFILES $PROGRAMFILES64
  !define VREDISTR_EXE_2015 "vc_redist.x86.exe" ;x64 or x86 (32b)
  
  
  !define VREDISTR_URL_2015 "https://download.microsoft.com/download/9/3/F/93FCF1E7-E6A4-478B-96E7-D4B285925B00/${VREDISTR_EXE_2015}" 
  !define MUI_VERSION "2.0.2"
  !define VREDISTR_KEY_2015 "SOFTWARE\Microsoft\DevDiv\VC\Servicing\14.0"
  
  ;Icon
  ;!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install-blue.ico"
  ;!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall-blue.ico"
  !define MUI_ICON "icon.ico"
  ;!define MUI_UNICON "Setup.ico"
 
  ; MUI Settings / Wizard
  !define MUI_WELCOMEFINISHPAGE_BITMAP "setup_lat3.bmp"
  ;!define MUI_HEADERIMAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Header\setup_lat.bmp"
  ;!define MUI_HEADERIMAGE_UNBITMAP "${NSISDIR}\Contrib\Graphics\Header\setup_lat.bmp"
  !define MUI_UNWELCOMEFINISHPAGE_BITMAP "setup_lat2.bmp"
 
  ;Name and file
  Name "GT Text"
  OutFile "GTText_Setup_${MUI_VERSION}_${MUI_ARQUITECTURE}.exe"
  BrandingText /TRIMCENTER "(c) 2016 GT Text ${MUI_VERSION}"

  ;Default installation folder. use 64 instead for 64 bit
  InstallDir "${MUI_PROGRFILES}\Softocr"
  
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\Softocr" ""

  ;Request application privileges for Windows Vista
  RequestExecutionLevel admin
  
;Macros
!include LogicLib.nsh
!include WinMessages.nsh
 
!macro ShellExecWait verb app param workdir show exitoutvar ;only app and show must be != "", every thing else is optional
#define SEE_MASK_NOCLOSEPROCESS 0x40 
System::Store S
System::Call '*(&i60)i.r0'
System::Call '*$0(i 60,i 0x40,i $hwndparent,t "${verb}",t $\'${app}$\',t $\'${param}$\',t "${workdir}",i ${show})i.r0'
System::Call 'shell32::ShellExecuteEx(ir0)i.r1 ?e'
${If} $1 <> 0
	System::Call '*$0(is,i,i,i,i,i,i,i,i,i,i,i,i,i,i.r1)' ;stack value not really used, just a fancy pop ;)
	System::Call 'kernel32::WaitForSingleObject(ir1,i-1)'
	System::Call 'kernel32::GetExitCodeProcess(ir1,*i.s)'
	System::Call 'kernel32::CloseHandle(ir1)'
${EndIf}
System::Free $0
!if "${exitoutvar}" == ""
	pop $0
!endif
System::Store L
!if "${exitoutvar}" != ""
	pop ${exitoutvar}
!endif
!macroend

;--------------------------------
;Variables

  Var StartMenuFolder
 
;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING
  !define MUI_FINISHPAGE_RUN
  !define MUI_FINISHPAGE_RUN_TEXT $(RunTxt)
  !define MUI_FINISHPAGE_RUN_FUNCTION "LaunchProgram"
  !define MUI_FINISHPAGE_LINK "SoftOCR.com"
  !define MUI_FINISHPAGE_LINK_LOCATION $(URLLink)

  

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "LicenseFree.txt"
 ; !insertmacro MUI_PAGE_COMPONENTS
  ;multiuser 
  !insertmacro MULTIUSER_PAGE_INSTALLMODE
  !insertmacro MUI_PAGE_DIRECTORY
  
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\Softocr" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Softocr"
  
  !define MUI_PRODUCT "GTText"
  !define MUI_PUBLISHER "SoftOCR"
  !define MUI_URL $(URLLink)
  
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH
  
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

 
  ;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"
  !insertmacro MUI_LANGUAGE "Spanish"
  !insertmacro MUI_LANGUAGE "French"
  
LangString URLLink ${LANG_ENGLISH} "http://www.softocr.com/home/"
LangString URLLink ${LANG_FRENCH} "http://www.softocr.com/fr/accueil/"
LangString URLLink ${LANG_SPANISH} "http://www.softocr.com/es/inicio/"

LangString RunTxt ${LANG_ENGLISH} "Run GT Text"
LangString RunTxt ${LANG_FRENCH} "Exécuter GT Text"
LangString RunTxt ${LANG_SPANISH} "Ejecutar GT Text"




;--------------------------------
;Installer Sections

Section "Install Section" SecInst

  ;Close GT Text
  ${nsProcess::CloseProcess} "GT.exe" $R0
  
  ReadRegStr $1 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "UninstallString"
  ;ExecWait '"$1" /S'
  !insertmacro ShellExecWait "" "$1" "/S" "" ${SW_HIDE} $0

  ${IfNot} $1 == ""
   Sleep 2000
  ${EndIf}
  
  SetRegView ${MUI_ARQUITECTURE}

  ;Uninstall previous versions
  Push "{C8187D08-DC8E-4382-9AEB-00F311C119F9}"
  Call UninstallOtherVersions
  Push "{1280A24A-6BAA-4E22-8F59-01B6551B47C5}"
  Call UninstallOtherVersions
  

  ;Check redistributables
  ClearErrors
  EnumRegKey $0 HKLM "${VREDISTR_KEY_2015}" 0

  IfErrors 0 installed

  # key does not exist
  ;not installed, so run the installer

  StrCpy $2 "$TEMP\${VREDISTR_EXE_2015}"
	 
  retry:
  inetc::get /caption "Downloading Microsoft Redistributable VC2015" /popup "" "${VREDISTR_URL_2015}" $2 /end
  Pop $0 # return value = exit code, "OK" if OK
  StrCmp $0 "OK" dlok
  MessageBox MB_ABORTRETRYIGNORE|MB_ICONEXCLAMATION  "There has been \
  a problem downloading Microsoft VC Redistributable 2015.$\nPlease, check your connection and try again." IDRETRY retry IDIGNORE installed
  Abort
  dlok:
  !insertmacro ShellExecWait "" "$2" "/q /norestart" "" ${SW_SHOW} $1
  Delete "$TEMP\${VREDISTR_EXE_2015}"

  installed:
  
  ReadENVStr $R0 ALLUSERSPROFILE
  SetOutPath "$R0\Softocr"
  File /oname=GT.exe "GTTextFiles${MUI_ARQUITECTURE}\GT.exe"
  File /oname=gl-000006664-20090126T000000000-0000.xml "GTTextFiles${MUI_ARQUITECTURE}\gl-000006664-20090126T000000000-0000.xml" 
  File /oname=gt-00006664-20081107T11432018-0815.xml "GTTextFiles${MUI_ARQUITECTURE}\gt-00006664-20081107T11432018-0815.xml"
  File /oname=pc-00006664-20081120T12402926-4796.xml "GTTextFiles${MUI_ARQUITECTURE}\pc-00006664-20081120T12402926-4796.xml" 
  File /oname=tesseract-ocr-language-2.0.0.exe "GTTextFiles${MUI_ARQUITECTURE}\tesseract-ocr-language-2.0.0.exe" 
  File /oname=liblept171.dll "GTTextFiles${MUI_ARQUITECTURE}\liblept171.dll"
  File /oname=libtesseract304.dll "GTTextFiles${MUI_ARQUITECTURE}\libtesseract304.dll"
  File /oname=GTENU.dll "GTTextFiles${MUI_ARQUITECTURE}\GTENU.dll"
  File /oname=GTESP.dll "GTTextFiles${MUI_ARQUITECTURE}\GTESP.dll"
  File /oname=GTFRA.dll "GTTextFiles${MUI_ARQUITECTURE}\GTFRA.dll"
  

  RMDir /r "$INSTDIR\*.*"
  RMDir "$INSTDIR"
  
  SetOutPath "$INSTDIR\tessdata"
  File /oname=eng.traineddata "GTTextFiles${MUI_ARQUITECTURE}\eng.traineddata"
 
  SetOutPath "$INSTDIR"
  File /oname=GT.exe "GTTextFiles${MUI_ARQUITECTURE}\GT.exe" 
  File /oname=gl-000006664-20090126T000000000-0000.xml "GTTextFiles${MUI_ARQUITECTURE}\gl-000006664-20090126T000000000-0000.xml"  
  File /oname=gt-00006664-20081107T11432018-0815.xml "GTTextFiles${MUI_ARQUITECTURE}\gt-00006664-20081107T11432018-0815.xml"  
  File /oname=pc-00006664-20081120T12402926-4796.xml "GTTextFiles${MUI_ARQUITECTURE}\pc-00006664-20081120T12402926-4796.xml"
  File /oname=tesseract-ocr-language-2.0.0.exe "GTTextFiles${MUI_ARQUITECTURE}\tesseract-ocr-language-2.0.0.exe"
  File /oname=liblept171.dll "GTTextFiles${MUI_ARQUITECTURE}\liblept171.dll" 
  File /oname=libtesseract304.dll "GTTextFiles${MUI_ARQUITECTURE}\libtesseract304.dll"
  File /oname=GTENU.dll "GTTextFiles${MUI_ARQUITECTURE}\GTENU.dll"
  File /oname=GTESP.dll "GTTextFiles${MUI_ARQUITECTURE}\GTESP.dll"
  File /oname=GTFRA.dll "GTTextFiles${MUI_ARQUITECTURE}\GTFRA.dll"
    
  
  
  
  ;Store installation folder
  WriteRegStr HKCU "Software\Softocr" "" $INSTDIR
  
  ;Write windows uninstall
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "DisplayName" "${MUI_PRODUCT}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "UninstallString" "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "DisplayIcon" "$INSTDIR\GT.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "DisplayVersion" "${MUI_VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "Publisher" "${MUI_PUBLISHER}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "URLInfoAbout" "${MUI_URL}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "ModifyPath" "$INSTDIR\tesseract-ocr-language-2.0.0.exe"
  ${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2 
  System::Int64Op $0 * 2
  Pop $0
  IntFmt $0 "0x%08X" $0
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "EstimatedSize" $0

  
  ;Open on start init
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Run" "GTText" "$\"$INSTDIR\GT.exe$\" /systray"
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
	
	
    CreateShortcut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
	CreateShortcut "$SMPROGRAMS\$StartMenuFolder\GT Text.lnk" "$INSTDIR\GT.exe"
	
	CreateShortCut "$DESKTOP\GT Text.lnk" "$INSTDIR\GT.exe" "" "$INSTDIR\GT.exe" 0
    CreateShortCut "$QUICKLAUNCH\GT Text.lnk" "$INSTDIR\GT.exe" "" "$INSTDIR\GT.exe" 0
  
  !insertmacro MUI_STARTMENU_WRITE_END
  

SectionEnd


;--------------------------------
;Descriptions
/*
  ;Language strings
  LangString DESC_SecDummy ${LANG_ENGLISH} "A test section."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecInst} $(DESC_SecInst)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END*/
 
;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...
  SetRegView ${MUI_ARQUITECTURE}

  ${nsProcess::CloseProcess} "GT.exe" $R0
  
  RMDir /r "$INSTDIR\*.*"
  RMDir $INSTDIR
  
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
    
  Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\GT Text.lnk"
  RMDir "$SMPROGRAMS\$StartMenuFolder"
  
  ;If is silent uninstall, we assume it is updating to a new one still tesseract 3.04. So we keep downloaded languages
  ${IfNot} ${Silent}
  RMDir /r "$APPDATA\Softocr\*.*"
  RMDir "$APPDATA\Softocr"
  ${EndIf}
  
  RMDir /r "$LOCALAPPDATA\Softocr\*"
  RMDir "$LOCALAPPDATA\Softocr"
  
  
  ReadENVStr $R0 ALLUSERSPROFILE
  RMDir /r "$R0\Softocr\*.*"
  RMDir "$R0\Softocr"
  
  
  DeleteRegKey /ifempty HKCU "Software\Softocr"
  
  ;Delete Uninstaller And Unistall Registry Entries
  DeleteRegKey HKLM "SOFTWARE\${MUI_PRODUCT}"
  DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}"

  ;Delet startup reg
  DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Run" "GTText"
  Delete "$DESKTOP\GT Text.lnk"
  Delete "$QUICKLAUNCH\GT Text.lnk"

SectionEnd

Function LaunchProgram
	 ExecShell "open" "$INSTDIR\GT.exe" "" SW_SHOWMAXIMIZED
FunctionEnd

Function un.onInit
  ;multiuser 
  !insertmacro MULTIUSER_UNINIT
   ;!insertmacro SELECT_UNSECTION Main ${UNSEC0000}
   ;!insertmacro MUI_UNGETLANGUAGE
FunctionEnd

Function .onInit
  Call PreventMultipleInstances
  
   ;multiuser 
   !insertmacro MULTIUSER_INIT
   ; Make selection based on System language ID
    System::Call 'kernel32::GetSystemDefaultLangID() i .r0'
    ;http://msdn.microsoft.com/en-us/library/dd318693%28v=VS.85%29.aspx
    StrCmp $0 "1078" Afrikaans
    StrCmp $0 "1052" Albanian
	StrCmp $0 "1118" Amharic
    StrCmp $0 "5121" Arabic
	StrCmp $0 "1101" Assamese
    StrCmp $0 "1068" Azerbaijani
	StrCmp $0 "2092" Azeri_cyrilic
    StrCmp $0 "1069" Basque
    StrCmp $0 "1059" Belarusian
    StrCmp $0 "1093" Bengali
	StrCmp $0 "5146" Bosnian
    StrCmp $0 "1026" Bulgarian
    StrCmp $0 "1027" Catalan
    StrCmp $0 "1116" Cherokee
    StrCmp $0 "31748" Chinese_tra
    StrCmp $0 "4" Chinese_sim
    StrCmp $0 "26" Croatian
    StrCmp $0 "1029" Czech
    StrCmp $0 "1030" Danish
    StrCmp $0 "2067" Dutch
    StrCmp $0 "1061" Estonian
	StrCmp $0 "1110" Galician
    StrCmp $0 "3079" German
	StrCmp $0 "1095" Gujarati
    StrCmp $0 "1032" Greek
    StrCmp $0 "1035" Finnish
    StrCmp $0 "2060" French
    StrCmp $0 "1037" Hebrew
    StrCmp $0 "1081" Hindi
    StrCmp $0 "1038" Hungarian
    StrCmp $0 "1039" Icelandic
    StrCmp $0 "1057" Indonesian
	StrCmp $0 "2108" Irish
    StrCmp $0 "1040" Italian
    StrCmp $0 "1041" Japanese
    StrCmp $0 "1099" Kannada
    StrCmp $0 "1042" Korean
    StrCmp $0 "1062" Latvian
    StrCmp $0 "1063" Lithuanian
    StrCmp $0 "1071" Macedonian
    StrCmp $0 "1100" Malayalam
    StrCmp $0 "2110" Malay
    StrCmp $0 "1082" Maltese
    StrCmp $0 "1044" Norwegian
    StrCmp $0 "1045" Polish
    StrCmp $0 "1046" Portuguese
    StrCmp $0 "1048" Romanian
    StrCmp $0 "1049" Russian
    StrCmp $0 "1051" Slovak
    StrCmp $0 "1060" Slovenian
    StrCmp $0 "11274" Spanish
    StrCmp $0 "2074" Serbian
    StrCmp $0 "1089" Swahili
    StrCmp $0 "2077" Swedish
    StrCmp $0 "1097" Tamil
    StrCmp $0 "1098" Telugu
    StrCmp $0 "1054" Thai
	StrCmp $0 "2129" Tibetan
    StrCmp $0 "1055" Turkish
    StrCmp $0 "1058" Ukrainian
    StrCmp $0 "1066" Vietnamese
	StrCmp $0 "1106" Welsh

    Goto lang_end

    Afrikaans: !insertmacro SelectSection ${SecLang_afr}
            Goto lang_end
    Albanian: !insertmacro SelectSection ${SecLang_sqi}
            Goto lang_end
	Amharic: !insertmacro SelectSection ${SecLang_amh}
            Goto lang_end
    Arabic: !insertmacro SelectSection ${SecLang_ara}
            Goto lang_end
	Assamese: !insertmacro SelectSection ${SecLang_asm}
            Goto lang_end		
    Azerbaijani: !insertmacro SelectSection ${SecLang_aze}
            Goto lang_end
	Azeri_cyrilic: !insertmacro SelectSection ${SecLang_aze_cyrl}
            Goto lang_end
    Basque: !insertmacro SelectSection ${SecLang_eus}
            Goto lang_end
    Belarusian: !insertmacro SelectSection ${SecLang_bel}
            Goto lang_end
    Bengali: !insertmacro SelectSection ${SecLang_ben}
            Goto lang_end
	Bosnian: !insertmacro SelectSection ${SecLang_bos}
            Goto lang_end
    Bulgarian: !insertmacro SelectSection ${SecLang_bul}
            Goto lang_end
    Catalan: !insertmacro SelectSection ${SecLang_cat}
            Goto lang_end
    Cherokee: !insertmacro SelectSection ${SecLang_chr}
            Goto lang_end
    Chinese_tra: !insertmacro SelectSection ${SecLang_chi_tra}
            Goto lang_end
    Chinese_sim: !insertmacro SelectSection ${SecLang_chi_sim}
            Goto lang_end
    Croatian: !insertmacro SelectSection ${SecLang_hrv}
            Goto lang_end
    Czech: !insertmacro SelectSection ${SecLang_ces}
            Goto lang_end
    Danish: !insertmacro SelectSection ${SecLang_dan}
            Goto lang_end
    Dutch: !insertmacro SelectSection ${SecLang_nld}
            Goto lang_end
    Estonian: !insertmacro SelectSection ${SecLang_hrv}
            Goto lang_end
	Galician: !insertmacro SelectSection ${SecLang_glg}
            Goto lang_end		
    German: !insertmacro SelectSection ${SecLang_deu}
            Goto lang_end
	Gujarati: !insertmacro SelectSection ${SecLang_guj}
            Goto lang_end
    Greek: !insertmacro SelectSection ${SecLang_ell}
            !insertmacro SelectSection ${SecLang_grc}
            Goto lang_end
    Finnish: !insertmacro SelectSection ${SecLang_fin}
            !insertmacro SelectSection ${SecLang_frm}
            Goto lang_end
    French: !insertmacro SelectSection ${SecLang_fra}
            Goto lang_end
    Hebrew: !insertmacro SelectSection ${SecLang_heb}
            Goto lang_end
    Hungarian: !insertmacro SelectSection ${SecLang_hin}
            Goto lang_end
    Hindi: !insertmacro SelectSection ${SecLang_hun}
            Goto lang_end
    Icelandic: !insertmacro SelectSection ${SecLang_isl}
            Goto lang_end
    Indonesian: !insertmacro SelectSection ${SecLang_ind}
            Goto lang_end
	Irish: !insertmacro SelectSection ${SecLang_gle}
            Goto lang_end
    Italian: !insertmacro SelectSection ${SecLang_ita}
            !insertmacro SelectSection ${SecLang_ita_old}
            Goto lang_end
    Japanese: !insertmacro SelectSection ${SecLang_jpn}
            Goto lang_end
    Kannada: !insertmacro SelectSection ${SecLang_kan}
            Goto lang_end
    Korean: !insertmacro SelectSection ${SecLang_kor}
            Goto lang_end
    Latvian: !insertmacro SelectSection ${SecLang_lav}
            Goto lang_end
    Lithuanian: !insertmacro SelectSection ${SecLang_lit}
            Goto lang_end
    Macedonian: !insertmacro SelectSection ${SecLang_mkd}
            Goto lang_end
    Malayalam: !insertmacro SelectSection ${SecLang_msa}
            Goto lang_end
    Malay: !insertmacro SelectSection ${SecLang_mal}
            Goto lang_end
    Maltese: !insertmacro SelectSection ${SecLang_mlt}
            Goto lang_end
    Norwegian: !insertmacro SelectSection ${SecLang_nor}
            Goto lang_end
    Polish: !insertmacro SelectSection ${SecLang_pol}
            Goto lang_end
    Portuguese: !insertmacro SelectSection ${SecLang_por}
            Goto lang_end
    Romanian: !insertmacro SelectSection ${SecLang_ron}
            Goto lang_end
    Russian: !insertmacro SelectSection ${SecLang_rus}
            Goto lang_end
    Slovak: !insertmacro SelectSection ${SecLang_slk}
            Goto lang_end
    Slovenian: !insertmacro SelectSection ${SecLang_slv}
            Goto lang_end
    Spanish: !insertmacro SelectSection ${SecLang_spa}
            !insertmacro SelectSection ${SecLang_spa_old}
            Goto lang_end
    Serbian: !insertmacro SelectSection ${SecLang_srp}
            Goto lang_end
    Swahili: !insertmacro SelectSection ${SecLang_swa}
            Goto lang_end
    Swedish: !insertmacro SelectSection ${SecLang_swe}
            Goto lang_end
    Tamil: !insertmacro SelectSection ${SecLang_tam}
            Goto lang_end
    Telugu: !insertmacro SelectSection ${SecLang_tel}
            Goto lang_end
    Thai: !insertmacro SelectSection ${SecLang_tha}
            Goto lang_end
	Tibetan: !insertmacro SelectSection ${SecLang_bot}
            Goto lang_end		
    Turkish: !insertmacro SelectSection ${SecLang_tur}
            Goto lang_end
    Ukrainian: !insertmacro SelectSection ${SecLang_ukr}
            Goto lang_end
    Vietnamese: !insertmacro SelectSection ${SecLang_vie}
			Goto lang_end
	Welsh: !insertmacro SelectSection ${SecLang_cym}

    lang_end:	
FunctionEnd

; Prevent running multiple instances of the installer
Function PreventMultipleInstances
  Push $R0
  System::Call 'kernel32::CreateMutexA(i 0, i 0, t ${PRODUCT_NAME}) ?e'
  Pop $R0
  StrCmp $R0 0 +3
    MessageBox MB_OK|MB_ICONEXCLAMATION "The installer is already running." /SD IDOK
    Abort
  Pop $R0
FunctionEnd

Function UninstallOtherVersions
  SetRegView ${MUI_ARQUITECTURE}
  Pop $R0
  ReadRegStr $1 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$R0" "UninstallString"
  Push " " ;divider char
  Push $1 ;input string
  Call SplitFirstStrPart
  Pop $R0 ;1st part ["string1"]
  Pop $R1 ;rest ["string2|string3|string4|string5"]
 
  ;We take out the /I parameter and /X, uninstall in msi installer. We uninstall silently
  StrCpy $1 $R1 "" 2
  !insertmacro ShellExecWait "" "$R0" "/x $1 /quiet" "" ${SW_HIDE} $1
FunctionEnd

Function .onInstSuccess
  ExecShell "open" "$INSTDIR\GT.exe" "/traybaloon" SW_SHOWNORMAL
  ExecShell "open" $(URLLink) SW_SHOWMAXIMIZED
FunctionEnd


Function SplitFirstStrPart
  Exch $R0
  Exch
  Exch $R1
  Push $R2
  Push $R3
  StrCpy $R3 $R1
  StrLen $R1 $R0
  IntOp $R1 $R1 + 1
  loop:
    IntOp $R1 $R1 - 1
    StrCpy $R2 $R0 1 -$R1
    StrCmp $R1 0 exit0
    StrCmp $R2 $R3 exit1 loop
  exit0:
  StrCpy $R1 ""
  Goto exit2
  exit1:
    IntOp $R1 $R1 - 1
    StrCmp $R1 0 0 +3
     StrCpy $R2 ""
     Goto +2
    StrCpy $R2 $R0 "" -$R1
    IntOp $R1 $R1 + 1
    StrCpy $R0 $R0 -$R1
    StrCpy $R1 $R2
  exit2:
  Pop $R3
  Pop $R2
  Exch $R1 ;rest
  Exch
  Exch $R0 ;first
FunctionEnd

