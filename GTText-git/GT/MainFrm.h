// MainFrm.h: interfaz de la clase CMainFrame
//
#include "MainWnd.h"
#include <map>

class CSideBar:public CDialogBar
{
public:
 CSideBar() {};
 virtual ~CSideBar() {};
 
 protected:
	DECLARE_MESSAGE_MAP()
public:
 afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};


enum
{
	TRAY_MILISECONDS_RESTART = 300000//300000
};

enum RegReturn { ALTC, CTRALTC, NOHOTKEY };

class CMainFrame : public CFrameWnd
{

		
protected: // Crear sólo a partir de serialización
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Atributos
protected:
	CSplitterWnd m_wndSplitter;
	CSplitterWnd m_wndSplitterXML;
	CMainWindow m_mainWindow;
	CDialogBar  m_wndBrightBar;
	CDialogBar  m_wndSensitivityBar;
	CSideBar  m_wndZoomBar;
	CSideBar  m_wndPenBar;
	CSideBar m_wndLanguageBar;
	//bit x1 enabling
	//bit 1x visible
	int isSelBrightActive;
	int isSelSensActive;
	int isZoomActive;
	int isPenActive;
	bool m_isLoaded;
	bool m_isAltAvailable;

// Reemplazos
public:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	void AddBrightBar(bool isAdded = true);
	void AddSensitivityBar(bool isAdded = true);
	void AddZoom(bool isAdded);
	void AddPen(bool isAdded);
	void HideToolBars();
	BOOL HideTrayIcon();
	RegReturn RegisterHotKeyAltC();
	void UnregisterHotKeyAltC();
	void OnSettingschangeLanguage();
	void InitRestartTimer();
	void StopInitTimer();
	bool IsFirstTimeLauched() { return m_firstTimeLaunch; };
	void ShowBaloonTray(CString message);

// Implementación
public:
	virtual ~CMainFrame();


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif


protected:  // Miembros incrustados de la barra de control
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CToolBar    m_wndImageBar;
// Helper functions
private:
	CString GetStartOCRLanguage();
	std::map<int,CString> m_sysTrayLanguages;
// Funciones de asignación de mensajes generadas
protected:
	HICON m_hIcon;

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()

	NOTIFYICONDATA	m_nid;			// struct for Shell_NotifyIcon args
	UINT_PTR m_timer;				// timer to restart gttext tray clean after inactivity
	bool m_firstTimeLaunch;

	void OnTrayContextMenu();
public:
	afx_msg void OnViewImagebar();
	afx_msg void OnUpdateViewImagebar(CCmdUI *pCmdUI);
	afx_msg void OnViewSelectionbright();
	afx_msg void OnUpdateViewSelectionbright(CCmdUI *pCmdUI);
	afx_msg void OnNMReleasedcaptureBrightSlider(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnViewSensitivity();
	afx_msg void OnUpdateViewSensitivity(CCmdUI *pCmdUI);
	afx_msg void OnNMReleasedcaptureOnfillSlider(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinsens(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnViewZoombar();
	afx_msg void OnUpdateViewZoombar(CCmdUI *pCmdUI);
	afx_msg void OnCbnSelchangeCombozoom();
	afx_msg void OnCombozoom(UINT nID);
	afx_msg void OnUpdateViewBrushsizebar(CCmdUI *pCmdUI);
	afx_msg void OnCbnSelchangeCombopen();
	afx_msg void OnBnClickedCheckborder();
	afx_msg void OnBnClickedExt();
	afx_msg void OnBnClickedToAllImage();
	afx_msg void OnViewBrushsizebar();

	afx_msg void OnUpdateFileFromscannerorcamera(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFileFromscreen(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFileFromrectangle(CCmdUI *pCmdUI);
	afx_msg void OnCbnDropdownLanguages();
	afx_msg void OnNMClickSyslinklang(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnViewLanguagebar();
	afx_msg void OnUpdateViewLanguagebar(CCmdUI *pCmdUI);

	afx_msg void OnCbnSelchangeLanguages();

	afx_msg void OnAppAbout();
	afx_msg void OnAppExit();
	afx_msg void OnTryExit();
	afx_msg void OnAppOpen();
	afx_msg LRESULT OnTrayNotify(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnHotKey(WPARAM wp, LPARAM lp);
	afx_msg void OnFileMenuLanguage(UINT nID);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

};



