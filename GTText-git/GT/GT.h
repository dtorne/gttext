// GT.h: archivo de encabezado principal para la aplicación GT
//
#pragma once

#ifndef __AFXWIN_H__
	#error "incluir 'stdafx.h' antes de incluir este archivo para PCH"
#endif

#include "resource.h"       // Símbolos principales
#include "Xmltree_i.h"
#include "./Scanner/MainWnd.h"


// CGTApp:
// Consulte la sección GT.cpp para obtener información sobre la implementación de esta clase
//
enum ImportSource {
	clipboard,
	scanner,
	snapshot,
	imagefile,
	noimport,
	rectangle,
	dragged,
	indetermined
};

class CGTApp : public CWinAppEx
{
public:
	CGTApp();


// Reemplazos
public:
	virtual BOOL InitInstance();

// Implementación
	afx_msg void OnAppAbout();

	DECLARE_MESSAGE_MAP()
private:
	BOOL m_bATLInited;
private:
	BOOL InitATL();
	ImportSource m_importType;
	CMainWindow mainWindow;
	bool m_active;
	bool m_initSystemTray;
	bool m_stopInitSystemTray;
	bool m_baloonOnStart;

public:
	afx_msg void OnFileFromclipboard();
	ImportSource GetImportType(){return m_importType;};
	CMainWindow* GetScannerManager(){return &mainWindow;};
	void SetImportType(ImportSource import){m_importType = import;};
	void SetActive(bool active){m_active = active;};
	bool GetActive(){return m_active;};
	bool IsSystemTrayInit() { return m_initSystemTray; };
	bool IsTrayInitStopped() { return m_stopInitSystemTray; };
	void StopInitSystemTray(bool stop) { m_stopInitSystemTray = stop; };
	bool IsBaloonOnStart() { return m_baloonOnStart; };
	void SetBaloonOnStart(bool showBaloonOnStart) { m_baloonOnStart = showBaloonOnStart; };
	
	afx_msg void OnUpdateFileFromclipboard(CCmdUI *pCmdUI);
	afx_msg void OnFileFromscannerorcamera();
	afx_msg void OnFileFromscreen();
	afx_msg void OnFileFromrectangle();
	afx_msg void OnHelpTutorials();
	afx_msg void OnHelpOcrlanguages();
	afx_msg void OnHelpTechnicalsupport();
	afx_msg void OnHelpSendfeedback();
};

extern CGTApp theApp;